
		.data 0x10010000
var1: 	.word 0x83
var2: 	.word 0x104
var3:	.word 0x111
var4:	.word 0x119

first:	.byte 'j'
last:	.byte 'c'
		.text
		.globl main

main:		addu $s0, $ra, $0	# save $31 in $16
		li $t0, 1
		addu $t1, $t0, $0
		la $t2, var1	# get address of var1
		la $t5, var4	# get address of var4
        li $v0,1    # code 1 == print integer
        la $a0, var1  # $a0 is address of var1
        syscall
        li $v0,1    # code 1 == print integer
        la $a0, var4  # $a0 is address of var4
        syscall
		lw $t3, var4	# save var1 value in register $t3
		lw $t4, var1	# save var4 value in register $t4
		sw $t3, 0($t2)	# store original var4 into var1 address
		sw $t4, 0($t5)	# store original var1 into var4 address
		la $t2, var2	# get var2 address
		la $t5, var3	# get var3 address
		lw $t3, var3	# save var3 in register $t3
		lw $t4, var2	# save var2 in register $t4
        li $v0,1    # code 1 == print integer
        la $a0, var2  # $a0 is address of var2
        syscall
        li $v0,1    # code 1 == print integer
        la $a0, var3  # $a0 is address of var3
        syscall
        li $v0,1    # code 1 == print integer
        la $a0, first  # $a0 is address of first
        syscall
        li $v0,1    # code 1 == print integer
        la $a0, last  # $a0 is address of last
        syscall
		sw $t3, 0($t2)	# store original var3 in var2 address
		sw $t4, 0($t5)	# store original var2 in var3 address

# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 	# return address back in $31
		jr $ra 		# return from main


