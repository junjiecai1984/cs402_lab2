
		.data 0x10010000
var1: 	.word 0x83
var2: 	.word 0x104
var3:	.word 0x111
var4:	.word 0x119

first:	.byte 'j'
last:	.byte 'c'
		.text
		.globl main

main:		addu $s0, $ra, $0	# save $31 in $16
		ori $8, $0, 1
		addu $t1, $t0, $0
		lui $10, 4097 # get address of var1
		lui $1, 4097	# get address of var4
		ori $13, $1, 12
        ori $2, $0, 1     # code 1 == print integer
        lui $4, 4097  # $a0 is address of var1
        syscall
        ori $2, $0, 1    # code 1 == print integer
        lui $1, 4097  # $a0 is address of var4
        ori $4, $1, 12
        syscall
		lui $1, 4097	# save var1 value in register $t3
		lw $11, 12($1)
		lui $1, 4097	# save var4 value in register $t4
		lw $12, 0($1)
		sw $t3, 0($t2)	# store original var4 into var1 address
		sw $t4, 0($t5)	# store original var1 into var4 address
		lui $1, 4097	# get var2 address
		ori $10, $1, 4
		lui $1, 4097	# get var3 address
		ori $13, $1, 8
		lui $1, 4097	# save var3 in register $t3
        lw $11, 8($1)
		lui $1, 4097	# save var2 in register $t4
		lw $12 4($1)
        ori $2, $0, 1   # code 1 == print integer
        lui $1, 4097  # $a0 is address of var2
        ori $4, $1, 4
        syscall
        ori $2, $0, 1    # code 1 == print integer
        lui $1, 4097  # $a0 is address of var3
        ori $4, $1, 8
        syscall
        ori $2, $0, 1    # code 1 == print integer
        lui $1, 4097  # $a0 is address of first
        ori $4, $1, 16
        syscall
        ori $2, $0, 1    # code 1 == print integer
        lui $1, 4097  # $a0 is address of last
        ori $4, $1, 17
        syscall
		sw $t3, 0($t2)	# store original var3 in var2 address
		sw $t4, 0($t5)	# store original var2 in var3 address

# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 	# return address back in $31
		jr $ra 		# return from main


