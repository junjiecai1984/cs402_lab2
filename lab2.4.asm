.data 0x10010000
	var1: .word 32	# var1 is a word with value 32
	var2: .word 10	# vvar1 is a word with value 10
.extern ext1 4		# ext1 is a word in external space
.extern	ext2 4		# ext2 is a word in external space
	
.text
.globl main	
	main: 
	lw $t0,var1	# load value of var1 to register t0
	lw $t1,var2	# load value of var2 to register t1
	
	sw $t0,ext1	# store value of var1 to address of ext1
	sw $t1,ext2	# store value of var2 to address of ext2

	#li $v0, 1	# system call for print_str
	#lw $a0, 0($t2)
	#syscall
	#lw $a0, 0($t3)
	#syscall
	li $v0,10	# system call for exit
	syscall		# exit